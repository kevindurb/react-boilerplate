var Dispatcher = require('flux').Dispatcher;
var constants = require('../constants/constants');
var assign = require('object-assign');

var dispatcher = assign(new Dispatcher(), {
  handleViewAction: function(action) {
    this.dispatch({
      source: constants.VIEW_ACTION,
      action: action
    });
  }
});

module.exports = dispatcher;
