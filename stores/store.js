var EventEmitter = require('events').EventEmitter;
var constants = require('../constants/constants');
var assign = require('object-assign');
var dispatcher = require('../dispatcher/dispatcher');

var store = assign({}, EventEmitter.prototype, {
});

dispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.actionType) {
    default:
      store.emit(constants.CHANGE_EVENT);
  }

  return true;
});

module.exports = store;
