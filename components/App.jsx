var React = require('react');
var constants = require('../constants/constants');
var store = require('../stores/store');
var actions = require('../actions/actions');

function getState() {
  return {
  };
}

var App = React.createClass({
  getInitialState: function() {
    return getState();
  },
  componentDidMount: function() {
    store.on(constants.CHANGE_EVENT, this.storeChanged);
  },
  componentWillUnmount: function() {
    store.off(constants.CHANGE_EVENT, this.storeChanged);
  },
  storeChanged: function() {
    this.setState(getState());
  },
  render: function() {
    return (
      <main>
      </main>
    )
  }
});

module.exports = App;
