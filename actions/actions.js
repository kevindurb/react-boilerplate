var dispatcher = require('../dispatcher/dispatcher');
var constants = require('../constants/constants');
module.exports = {
  doSomething: function() {
    dispatcher.handleViewAction({
      actionType: constants.SOMETHING
    });
  }
};
