var React = require('react');
var docReady = require('doc-ready');
var App = require('./components/App.jsx');

docReady(function() {
  React.render(
    <App />,
    document.body
  );
});
